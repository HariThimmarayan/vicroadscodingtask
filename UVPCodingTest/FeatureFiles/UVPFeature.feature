﻿Feature: UVPFeature

Scenario Outline: To verify the Second page header
	Given I'm on the Vicroads UVP page
	And enter the Step1 data based on the <Vehicle type>, <Passenger vehicle type> and <Permit Duration>
	When I click on the next button
	Then verify page2 header is Step 2 of 7 : Select permit type


	Examples: 
	| Vehicle type      | Passenger vehicle type | Permit Duration | 
	| Passenger vehicle | Sedan                  | 2 days          | 