﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UVPCodingTest.Pages
{
    public class Step1CalculateFeePage
    {
        IWebDriver _driver;
        #region Elements
        IWebElement ddVehicleType => _driver.FindElement(By.XPath("//select[contains(@id,'VehicleType_DDList')]"));
        IWebElement ddPassengerVehicle => _driver.FindElement(By.XPath("//select[contains(@id,'PassengerVehicleSubType_DDList')]"));
        IWebElement txtAddressVehicleGaraged => _driver.FindElement(By.XPath("//input[contains(@id,'AddressLine_SingleLine')]"));
        IWebElement txtPermitDate => _driver.FindElement(By.XPath("//input[contains(@id,'PermitStartDate_Date')]"));
        IWebElement ddPermitDuration => _driver.FindElement(By.XPath("//select[contains(@id,'PermitDuration_DDList')]"));
        IWebElement btnNext => _driver.FindElement(By.XPath("//input[contains(@id,'panel_btnNext')]"));
        #endregion

        #region Constructor
        public Step1CalculateFeePage (IWebDriver driver)
        {
            this._driver = driver;
        }
        #endregion

        #region Methods
        public void SelectVehicleType(String vehicleType)
        {
            SelectElement select = new SelectElement(ddVehicleType);
            select.SelectByText(vehicleType);
        }
        public void SelectPassengerVehicle(String passengerVehicle)
        {
            SelectElement choosePassengerVehicle = new SelectElement(ddPassengerVehicle);
            choosePassengerVehicle.SelectByText(passengerVehicle);
        }
        public void EnterAddressVehicleGaraged()
        {
            txtAddressVehicleGaraged.SendKeys("Unit 7 11 Sample Street, Broadmeadows VIC 3047");
        }
        public void EnterPermitDate()
        {
            txtPermitDate.Clear();
            txtPermitDate.SendKeys(DateTime.Now.ToString("dd/MM/yyyy"));
        }
        public void SelectPermitDuration(String permitdays)
        {
            SelectElement selectPermitDays = new SelectElement(ddPermitDuration);
            selectPermitDays.SelectByText(permitdays);

        }
        public Step2PermitPage ClickOnNextButton()
        {
            btnNext.Click();
            return new Step2PermitPage(_driver);
        }
        #endregion
    }
}
