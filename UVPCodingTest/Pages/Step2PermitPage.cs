﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UVPCodingTest.Pages
{
    public class Step2PermitPage
    {
        IWebDriver _driver;
        public Step2PermitPage(IWebDriver driver)
        {
            this._driver = driver;
        }
        IWebElement txtPage2Title => _driver.FindElement(By.XPath("//p[@class='progress-bar-title']"));

        public string GetPage2Title()
        {
            return txtPage2Title.Text;
        }

    }

}

