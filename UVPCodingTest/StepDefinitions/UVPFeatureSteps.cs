﻿using NUnit.Framework;
using OpenQA.Selenium;
using System;
using System.Configuration;
using TechTalk.SpecFlow;
using UVPCodingTest.Base;
using UVPCodingTest.Pages;

namespace UVPCodingTest.StepDefinitions
{
    [Binding]
    public class UVPFeatureSteps
    {
        IWebDriver _driver;
        Step1CalculateFeePage step1CalculatePage;
        Step2PermitPage step2PermitPage;


        [Given(@"I'm on the Vicroads UVP page")]
        public void GivenIMOnTheVicroadsUVPPage()
        {
            _driver = DriverControl.InitializeDriver(ConfigurationManager.AppSettings.Get("browser"));
            _driver.Navigate().GoToUrl(ConfigurationManager.AppSettings.Get("UVPUrl"));
        }
        
        [Given(@"enter the Step1 data based on the (.*), (.*) and (.*)")]
        public void GivenEnterTheStep1DataBasedOnThe(String vehicleType, String passengerVehicleType, string permitDuration)
        {
            step1CalculatePage = new Step1CalculateFeePage(DriverControl.Driver);
            step1CalculatePage.SelectVehicleType(vehicleType);
            step1CalculatePage.SelectPassengerVehicle(passengerVehicleType);
            step1CalculatePage.EnterAddressVehicleGaraged();
            step1CalculatePage.EnterPermitDate();
            step1CalculatePage.SelectPermitDuration(permitDuration);
        }
        
        [When(@"I click on the next button")]
        public void WhenIClickOnTheNextButton()
        {
            step1CalculatePage.ClickOnNextButton();
        }
        
        [Then(@"verify page2 header is (.*)")]
        public void ThenVerifyPageHeaderIsStepOfSelectPermitType(String page2header)
        {
            step2PermitPage = new Step2PermitPage(DriverControl.Driver);
            Assert.AreEqual(page2header, step2PermitPage.GetPage2Title(), "Header is not matching");
        }
        

    }
}
